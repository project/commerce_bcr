<?php

namespace Drupal\commerce_placetopay\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

class RedirectCheckoutPaymentForm extends BasePaymentOffsiteForm {

  /**
   * The checkout auth object.
   *
   * @var \Drupal\commerce_placetopay\CheckoutAuth
   */
  protected $checkout_auth;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_placetopay\Plugin\Commerce\PaymentGateway\RedirectCheckoutInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $extra = [
      'return_url' => $form['#return_url'],
      'cancel_url' => $form['#cancel_url']
    ];

    $redirect_url = $payment_gateway_plugin->checkoutAuthorize($payment, $extra);

    if (empty($redirect_url)) {
      throw new PaymentGatewayException(sprintf('Error processing the authetication.'));
    }


    return $this->buildRedirectForm($form, $form_state, $redirect_url, [], self::REDIRECT_GET);
  }

}
