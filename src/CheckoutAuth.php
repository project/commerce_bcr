<?php

namespace Drupal\commerce_placetopay;

use Drupal\Component\Utility\Random;
use GuzzleHttp\Exception\RequestException;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Perform the main API functions.
 */
class CheckoutAuth implements CheckoutAuthInterface
{

  /**
   * Utility to generate random strings.
   *
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;


  /**
   * The payment gateway plugin configuration.
   *
   * @var array
   */
  protected $config;

  /**
   * The process URL.
   *
   * @var string
   */
  protected $processURL;

  /**
   * The request Id.
   *
   * @var int
   */
  protected $requestId;

  /**
   * The tranKey generated per request.
   *
   * @var string
   */
  protected $tranKey;

  /**
   * The nonce generated per request.
   *
   * @var string
   */
  protected $nonce;

  /**
   * The seed generated per request.
   *
   * @var string
   */
  protected $seed;

  /**
   * Constructs a new CheckoutSdk object.
   */
  public function __construct(array $config)
  {
    $current_time = \Drupal::time()->getCurrentTime();
    $this->random = new Random();
    $this->config = $config;
    $this->seed = date('c', $current_time);
    $this->nonce = $this->random->string(15, TRUE);
    $this->tranKey = $this->generateTranKey();
  }

  /**
   * {@inheritdoc}
   */
  public function authorize(PaymentInterface $payment, array $extra) {
    $base_url = $this->getBaseURL();
    $nonceBase64 = base64_encode($this->nonce);
    $expiration = date('c', strtotime('8 hour'));
    $login_key = $this->config['login_key'];
    $customer = $payment->getOrder()->getCustomer();
    $address = $payment->getOrder()->getBillingProfile()->get('address')->first()->getValue();

    if ($address) {
      $address_data = [
        'street' => isset($address['address_line1']) ? $address['address_line1'] : '',
        'city' => isset($address['address_locality']) ? $address['address_locality'] : '',
        'state' => isset($address['administrative_area']) ? $address['administrative_area'] : '',
        'postalCode' => isset($address['postal_code']) ? $address['postal_code'] : '',
        'country' => isset($address['country_code']) ? $address['country_code'] : ''
      ];
    }

    if ($customer) {
      $buyer_data = [
        'document' => '', // Will be added on next releases.
        'documentType' => '', // Will be added on next releases.
        'name' => isset($address['given_name']) ? $address['given_name'] : '',
        'surname' => isset($address['family_name']) ? $address['family_name'] : '',
        'email' =>  $customer->getEmail(),
        'mobile' => '', // Will be added on next releases.
        'address' => isset($address_data) ? $address_data : []
      ];
    }

    // Build the request body.
    $data = [
      'locale' => 'es_CR',
      'auth' => ['login' => $login_key, 'tranKey' => $this->tranKey, 'nonce' => $nonceBase64, 'seed' => $this->seed],
      'payment' => [
        'reference' => $payment->getOrderId(),
        'description' => 'Test order',
        'amount' => [
          'currency' => $payment->getAmount()->getCurrencyCode(),
          'total' => $payment->getAmount()->getNumber()
        ],
        'allowPartial' => false,
      ],
      'buyer' => isset($buyer_data) ? $buyer_data : [],
      'expiration' => $expiration,
      'returnUrl' =>  $extra['return_url'],
      'cancelUrl' =>  $extra['cancel_url'],
      'ipAddress' =>  \Drupal::request()->getClientIp(),
      'userAgent' => $_SERVER['HTTP_USER_AGENT'],
    ];

    // Initialize the http client.
    $client = \Drupal::httpClient();

    try {
      $responseJson = $client->post($base_url . '/api/session', [
          'timeout' => 10,
          'body' => json_encode($data, JSON_FORCE_OBJECT),
          'headers' => [
            'Content-Type' => 'application/json'
          ]
        ])->getBody()->getContents();
      $response = json_decode($responseJson);
      if (isset($response->processUrl) && isset($response->requestId)) {
        $this->processURL = $response->processUrl;
        $this->requestId = $response->requestId;
      } else {
        \Drupal::messenger()->addError(t('Error reading the response object.'));
      }
    } catch (RequestException $ex) {
      $error =  json_decode($ex->getResponse()->getBody()->getContents());
      if (isset($error->status->message)) {
        \Drupal::messenger()->addError($error->status->message);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionInfo(string $request_id) {
    // Get necesary data.
    $base_url = $this->getBaseURL();
    $nonceBase64 = base64_encode($this->nonce);
    $login_key = $this->config['login_key'];

    // Build the request body.
    $data = [
      'auth' => [
        'login' => $login_key,
        'tranKey' => $this->tranKey,
        'nonce' => $nonceBase64,
        'seed' => $this->seed
      ]
    ];

    // Initialize the http client.
    $client = \Drupal::httpClient();

    try {
      $responseJson = $client->post($base_url . '/api/session/' . $request_id, [
        'timeout' => 10,
        'body' => json_encode($data, JSON_FORCE_OBJECT),
        'headers' => [
          'Content-Type' => 'application/json'
        ]
      ])->getBody()->getContents();
      $response = json_decode($responseJson);
      if ($response) {
        return $response;
      } else {
        \Drupal::messenger()->addError(t('Error reading the response object.'));
        return null;
      }
    } catch (RequestException $ex) {
      $error =  json_decode($ex->getResponse()->getBody()->getContents());
      if (isset($error->status->message)) {
        \Drupal::messenger()->addError($error->status->message);
      }
      return null;
    }
  }

  /**
   * Gets the API bass URL.
   * 
   * @return string
   *   The base URL.
   */
  private function getBaseURL() {
    $mode = $this->config['mode'];
    if ($mode === 'live') {
      return $this->config['live_endpoint_url'];
    } else {
      return $this->config['test_endpoint_url'];
    }
  }

  /**
   * Generate a tranKey to be sent on request.
   * 
   * @return string
   *   The tranKey generated.
   */
  private function generateTranKey() {
    if (isset($this->config['secret_key'])) {
      $secret_key = $this->config['secret_key'];
      $tranKey = base64_encode(sha1($this->nonce . $this->seed . $secret_key, true));
      return $tranKey;
    }
    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranKey() {
    return $this->tranKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessUrl() {
    return $this->processURL;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestId()
  {
    return $this->requestId;
  }

}
